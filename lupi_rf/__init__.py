from .lupi_rf import LupiRFClassifier

__version__ = 'v6.0.0'
__author__ = 'VencoreLabs'

__all__ = ['LupiRFClassifier']

from pkgutil import extend_path
__path__ = extend_path(__path__, __name__)  # type: ignore


